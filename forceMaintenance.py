#!/usr/bin/python
#########################################################################################
## Hour Logging System force all maintenance to run.
## This file forces maintenance to run upon file calling (execution)
##
## This file will solve problem of maintenance running upon system start up.
## Created by: Tony Lim
#########################################################################################

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!       Running this file will force tap out on miss tap outs.     !!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

import maintenance

maintenance.dailyMaintenance()
maintenance.insertNextMonthShifts()
maintenance.semiYearlyMaintenance()
maintenance.biweeklyMaintenance()
maintenance.monthlyMaintenance()