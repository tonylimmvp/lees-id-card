#!/usr/bin/python
#########################################################################################
## Hour Logging System force daily maintenance file.
## This file forces daily maintenance to run upon file calling (execution)
##
## Daily maintenance will send an email reminder of a missed tap outs to the
## receiver mailing list set on cfg.py
##
## Created by: Tony Lim
#########################################################################################

import maintenance

maintenance.dailyMaintenance()