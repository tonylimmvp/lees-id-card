#!/usr/bin/python
#########################################################################################
## Hour Logging System helper functions file.
## This file's purpose is to simplify the main running program.
## This file is the production version, emulating DataCore connection through cfg.py.
##
## Created by: Tony Lim
#########################################################################################
from datetime import datetime, timedelta, date
import holidays
import calendar
import logging
import pymysql.cursors
import emailing
import cfg

# Set RFID Event type to match the database.
dbEventTypeWorkShift = 1
dbEventTypeHoliday = 3
dbEventTypeVacation = 4
dbEventTypeDayOff = 6
dbEventTypeRFID = 9

# Grab System's Employee ID
systemEmployeeID = 1

# Enable logging and determine file.
logging.basicConfig(filename='logger.log',level=logging.DEBUG, format='%(asctime)s %(filename)s - %(levelname)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S')

#############################################################################
## Obtaining Today & Tomorrow Date and Time in datetime format
## Returning in the format: 2020-01-22 14:30:54.880967
#############################################################################
def get_tomorrow_datetime():
    today = datetime.now()
    tomorrow = today + timedelta(days=1)
    return tomorrow

def get_today_datetime():
    today = datetime.now()
    return today

#############################################################################
## Helper functions to get current hour/minute/second in string
#############################################################################
def getCurrentHour():
    currentTime = datetime.now()
    currentHour = currentTime.strftime("%H")
    return currentHour

def getCurrentMinute():
    currentTime = datetime.now()
    currentMinute = currentTime.strftime("%M")
    return currentMinute

def getCurrentSecond():
    currentTime = datetime.now()
    currentSecond = currentTime.strftime("%S")
    return currentSecond

#############################################################################
## Obtaining Today & Tomorrow Date and Time
## Returning in the format: X = datetime.date(2020, 1, 22)
## X.year -- to get year; return int
## X.month -- to get month; return int
## X.day -- to get day; return int
#############################################################################
def get_tomorrow_date():
    today = date.today()
    tomorrow = today + timedelta(days=1)
    return tomorrow

def get_today_date():
    today = date.today()
    return today

def getLastDayOfMonth():
    currentYear = get_today_date().year
    currentMonth = get_today_date().month
    lastDayOfMonthTuple = calendar.monthrange(currentYear, currentMonth)
    lastDayOfMonth = lastDayOfMonthTuple[1]
    return lastDayOfMonth

def getLastDayOfMonthByInput(currentYear, currentMonth):
    lastDayOfMonthTuple = calendar.monthrange(currentYear, currentMonth)
    lastDayOfMonth = lastDayOfMonthTuple[1]
    return lastDayOfMonth
    
#############################################################################
## Clearing LCD's text and turn on backlight.
#############################################################################    
def clearLCD():
    mylcd.lcd_clear(LCD_BACKLIGHT)
    
#############################################################################
## Convert Datetime format to seconds
## E.g.: 12:05:49 -> 43549 and return as int
## @param timestamp -- Timestamp in format %H:%M:%S to be converted to
##    seconds in int
#############################################################################
def convertTimeToSeconds(timestamp):
    timestampHour = timestamp.hour
    timestampMinute = timestamp.minute
    timestampSecond = timestamp.second
    
    hourInSeconds = convertHourToSeconds(timestampHour)
    minuteInSeconds = convertMinuteToSeconds(timestampMinute)

    totalDaySeconds = hourInSeconds + minuteInSeconds + timestampSecond
    return totalDaySeconds
 
def convertHourToSeconds(hour):
    totalSeconds = hour * 3600
    return totalSeconds

def convertMinuteToSeconds(minute):
    totalSeconds = minute * 60
    return totalSeconds

#############################################################################
## Convert seconds to datetime format where hours can be >= 24.
## E.g.: 120045 -> 33:20:45 and return as str
## @param seconds -- seconds to be converted to format %H:%M:%S
#############################################################################
def convertSecondsToTime(seconds):
    totalHour = int(seconds / 3600)
    totalHourRem = seconds % 3600
    totalMin = int(totalHourRem / 60)
    totalSecond = int(totalHourRem % 60)
    
    # If the digit is single, then add 0 ahead of it.
    # Return the string
    if (totalHour <= 9):
        totalHourStr = "0"+str(totalHour)
    else:
        totalHourStr = str(totalHour)
        
    if (totalMin <= 9):
        totalMinStr = "0"+str(totalMin)
    else:
        totalMinStr = str(totalMin)
        
    if (totalSecond <= 9):
        totalSecondStr = "0"+str(totalSecond)
    else:
        totalSecondStr = str(totalSecond)
    
    timeStrFormat = totalHourStr+":"+totalMinStr+":"+totalSecondStr

    return timeStrFormat

#############################################################################
## Insert Statutory Holidays Into Events
#############################################################################
def insertStatHolidays(db):
    nextYear = get_today_date().year
    fromDateRange = str(nextYear) + "-01" + "-01"
    toDateRange = str(nextYear) + "-12" + "-31"
    
    with db.cursor() as cursor:
        for date, name in sorted(holidays.CA(state='BC', years=nextYear).items()):
            
            # Replacing a single quotation with double single quotation
            # Used to bypass entering escape clauses in SQL
            nameEscape = name.replace("'", "''")
            
            # Perform a check whether the holiday is already in events table
            sql = "SELECT COUNT(*) FROM `events` WHERE (start BETWEEN CAST('%s' AS DATE) AND CAST('%s' AS DATE) AND (type_id = %d) AND title = '%s')"%(fromDateRange, toDateRange, dbEventTypeHoliday, nameEscape)
            cursor.execute(sql)
            sqlResult = cursor.fetchall()
            
            # If not, then insert the holiday into events schedule.
            if (sqlResult[0]['COUNT(*)'] == 0):
                insQuery = "INSERT INTO events (type_id, employee_id, title, start, all_day) VALUES (%d, %d, '%s', '%s', 1)"%(dbEventTypeHoliday, systemEmployeeID, nameEscape, date)
                cursor.execute(insQuery)
                db.commit()
                print("Inserting holidays to events schedule completed succesfully.")
                
            # Otherwise, the system should perform a check on the date.
            else:
                sql2 = "SELECT * FROM `events` WHERE (start BETWEEN CAST('%s' AS DATE) AND CAST('%s' AS DATE) AND (type_id = %d) AND title = '%s')"%(fromDateRange, toDateRange, dbEventTypeHoliday, nameEscape)
                cursor.execute(sql2)
                sql2Result = cursor.fetchone()
                rowID = sql2Result['id']
                
                # Convert sql2 string format to double digit.
                sql2ResultMonth = sql2Result['start'].month
                sql2ResultDay = sql2Result['start'].day
                if (sql2ResultMonth <= 9):
                    sql2ResultMonthStr = "0"+str(sql2ResultMonth)
                else:
                    sql2ResultMonthStr = str(sql2ResultMonth)
                    
                if (sql2ResultDay <= 9):
                    sql2ResultDayStr = "0"+str(sql2ResultDay)
                else:
                    sql2ResultDayStr = str(sql2ResultDay)
                
                sql2ResultDateStr = str(sql2Result['start'].year) + "-" + sql2ResultMonthStr + "-" + sql2ResultDayStr
                # If the date isn't the same, then have to update previous entry to new date.
                # If the date remains the same then ignore and do not update.
                if (sql2ResultDateStr != str(date)):
                    sql3 = "UPDATE `events` SET start = '%s', title = '%s' WHERE id = %d"%(date, nameEscape, rowID)
                    cursor.execute(sql3)
                    print(name + " updated to " + str(date) + " on event #: " + str(rowID))
                else:
                    print(name + " already exist on " + str(date) + " row #: " + str(rowID)) 
                        
#############################################################################
## Get Employee's Name from employee ID and return as string
## @param employee_id -- Employee's ID associated with the employees table
#############################################################################
def getEmployeeName(db, employee_id):
    with db.cursor() as cursor:
        getEmployeeNameQuery = "SELECT * FROM `employees` WHERE id = %d"%(employee_id)
        cursor.execute(getEmployeeNameQuery)
        employeeNameQueryResult = cursor.fetchone()
        employeeName = employeeNameQueryResult['name']
        
        return employeeName
    
#############################################################################
## Convert datetime format to string YYYY-MM-DD
## Works in conjunction with get_today_datetime() and/or get_today_date()
## @param datetimeInput -- datetime format
#############################################################################
def convertDatetimeToString(datetimeInput):
    dtYearStr = str(datetimeInput.year)
    dtMonth = datetimeInput.month
    dtDay = datetimeInput.day
    
    if (dtMonth <= 9):
        dtMonthStr = "0"+str(dtMonth)
    else:
        dtMonthStr = str(dtMonth)
    if (dtDay <= 9):
        dtDayStr = "0"+str(dtDay)
    else:
        dtDayStr = str(dtDay)
    
    dtStrFormat = dtYearStr + "-" + dtMonthStr + "-" + dtDayStr
    
    return dtStrFormat

#############################################################################
## Convert datetime format to string HH:MM:SS
## Works in conjunction with get_today_datetime()
## @param datetimeInput -- datetime format
#############################################################################
def convertDatetimeToTimeString(datetimeInput):
    dtYear = str(datetimeInput.hour)
    dtMonth = datetimeInput.minute
    dtDay = datetimeInput.second
    
    dtStrFormat = str(dtYear) + ":" + str(dtMonth) + ":" + str(dtDay)
    
    return dtStrFormat

#############################################################################
## Manual database check and throw exception upon connection error and return
## boolean value
## @param db - database connection specified via cfg.py
#############################################################################
def checkDBConnection(db):
    try:
        with db.cursor() as cursor:   
            cursor.execute("SELECT VERSION()")
            results = cursor.fetchone()
            
            # Check if anything at all is returned
            if results:
                logging.debug("Database connection check successful.")
                return True
            else:
                return False
        
    except:
        emailing.genericErrorEmail("Database Connection Error!", "System encountered an error with connection to the database system. Please confirm that the database is running properly.")
        logging.error("System encountered an error with database connection!")
        print("System encountered an error with database connection!")
        
    return False
