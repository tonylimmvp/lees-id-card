#!/usr/bin/python
#########################################################################################
## File to write card UID into DataCore's employee table.
## This file is the production version, emulating DataCore connection.
## Note: This software is designed to run on Python 3. As such, MySQLdb and
##       SimpleMFRC522 library cannot be used as they are not compatible
##       with Python 3.
##
## This RFID Reader only takes 13.56MHz frequency cards/tags. (#15365 / 15050)
## A 125 KHz tag (#150321) is also working with the reader.
## However 125 KHz button tag (#15020) does not work.
##
## Created by: Tony Lim
#########################################################################################
import pymysql.cursors
import cfg
import helperfunctions as helper
import sys
import mfrc522
import I2C_LCD_driver
import time
from time import sleep

timeoutTime = 30

# Create an object of the class MFRC522
MIFAREReader = mfrc522.MFRC522()

#############################################################################
## Read card and fill the card id into the matched place "CARD_ID"
## under the employee's name
## The card information will only be written if the user and the card is unique
## Cannot be duplicate user/card.
## If time out, the function will return NONE, else will return id number.
## @param full_name -- Full name of employee for the card to write onto.
## Note: Parameters are case sensitive.
#############################################################################
def writeCardInfo():
    db = cfg.connectToDatacore()
    
    with db.cursor() as cursor:
        # Display all active employees for user input.
        query3 = "SELECT * FROM `employees` WHERE isActive = 1"
        cursor.execute(query3)
        activeEmployees = cursor.fetchall()
        
        for row in activeEmployees:
            employeeID = int(row['id'])
            employeeName = str(row['name'])
            print("ID #: " + str(employeeID) + " - " + employeeName)
        
        # Grab user employee ID as user input
        inputEmployeeID = input('Please input the employee ID of the user: ')
        
        if (inputEmployeeID.isnumeric()):
            # Check whether the ID is valid in the employees table.
            query = "(SELECT COUNT(id), name FROM `employees` WHERE id = %d AND isActive = 1)"%(int(inputEmployeeID))
            cursor.execute(query)
            queryResult = cursor.fetchall()

            if(queryResult[0]["COUNT(id)"] == 0):
                print("Error: Employee doesn't exist / Inactive.")
                sys.exit(0)
                
            #If there is a unique user, then continue storing data
            elif(queryResult[0]["COUNT(id)"] == 1):
                
                inEmployeeName = helper.getEmployeeName(db, int(inputEmployeeID))
                print("Your input of " + str(inputEmployeeID) + " returns the user: " + inEmployeeName)
                userIn = input("Would you like to continue for this user? [yes/no]")
                if (userIn == "yes" or userIn == "YES" or userIn == "y" or userIn == "Y"):
                    
                    # Begin Reading Card and Obtain the Unique ID
                    print("Please tap the RFID card for " + inEmployeeName)
                    cardID = readCard()
                    print("UID: "+cardID)
                                
                    # Check if Card already exist
                    query2 = """SELECT COUNT(*) FROM `employees` WHERE card_id = %d"""%(int(cardID))
                    cursor.execute(query2)
                    queryResult2 = cursor.fetchall()
                    db.commit()
                                
                    if((queryResult2[0]["COUNT(*)"]) >= 1):
                        print("Error: Card already used.")
                        time.sleep(3)
                        sys.exit(0)
                        
                    else:
                        # Update Employee's Database Information with Card's Unique ID
                        sql = "UPDATE employees SET CARD_ID = %d WHERE id = %d"%(int(cardID), int(inputEmployeeID))
                        cursor.execute(sql)
                        db.commit()
                        
                        # Allow 3 seconds to show read data, proceed to default state.
                        print("Inserting card information to employee successful.")
                        time.sleep(3)
                        sys.exit(0)
                else:
                    print("Exiting program.")
                    sys.exit(0)
            else:
                print("Fatal error on writing card info")
                sys.exit(0)  
        else:
            print("Your input is not an integer.")
            sys.exit(0)
          
#############################################################################
## Card Reading State
#############################################################################
def readCard():
    
    print("Tap Card! System will timeout in " + str(timeoutTime) + " seconds.")
    
    initTime = int(time.time())
    nowTime = int(time.time())
    
    try:
        # Loop until X (check variable: timeoutTime) seconds expire
        while (initTime + timeoutTime != nowTime):
            
            # Used to keep incrementing nowTime to calculate elapsed time.
            nowTime = int(time.time())

            print("initTime: " + str(initTime))
            print("nowTime: " + str(nowTime))

            # Scan for cards
            (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

            # Get the UID of the card
            (status,uid) = MIFAREReader.MFRC522_Anticoll()

            # If we have the UID, continue
            if status == MIFAREReader.MI_OK:
                
                # Print UID into LCD display and program.
                cardID = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
                print("UID: "+cardID)
          
                # Allow 3 seconds to show read data, proceed to default state.
                time.sleep(3)
                
                return cardID

        # Call timeout state
        if (initTime + timeoutTime == nowTime):
            print("Exiting.")
            time.sleep(3)
            sys.exit(0)
        
    except KeyboardInterrupt:
        sys.exit(0)
        
############################################################################
## MAIN LOOP FUNCTION
############################################################################
writeCardInfo()
        