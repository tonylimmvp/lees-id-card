#!/usr/bin/python
#########################################################################################
## Hour Logging System force biweekly maintenance file.
## This file forces biweekly maintenance to run upon file calling (execution)
##
## The biweekly maintenance will fill missed tap outs and calculate
## biweekly work hour.
##
## Created by: Tony Lim
#########################################################################################

import maintenance

maintenance.biweeklyMaintenance()