#!/usr/bin/python
#########################################################################################
## Hour Logging System maintenance file.
## This file is the production version, emulating DataCore connection through cfg.py.
## Note: This software is designed to run on Python 3. As such, MySQLdb and
##       SimpleMFRC522 library cannot be used as they are not compatible
##       with Python 3.
##
## This RFID Reader only takes 13.56MHz frequency cards/tags. (#15365 / 15050)
## A 125 KHz tag (#150321) is also working with the reader.
## However 125 KHz button tag (#15020) does not work.
##
## Possible replacement for MySQLdb is PyMySQL please see documentation below:
## https://pypi.org/project/PyMySQL/#installation 
##
## Created by: Tony Lim
#########################################################################################
import cfg
import helperfunctions as helper
import sys
import calendar
import emailing
from datetime import datetime, timedelta
import logging

# Set RFID Event type to match the database.
dbEventTypeWorkShift = 1
dbEventTypeHoliday = 3
dbEventTypeVacation = 4
dbEventTypeModifiedShift = 5
dbEventTypeDayOff = 6
dbEventTypeEvent = 7
dbEventTypeRFID = 9

# Enable logging and determine file.
logging.basicConfig(filename='logger.log',level=logging.DEBUG, format='%(asctime)s %(filename)s - %(levelname)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S')

#############################################################################
## Daily Maintenance main function
#############################################################################
def dailyMaintenance():
    # Connect to datacore connection
    db = cfg.connectToDatacore()
    
    # Check database connection integrity
    if (helper.checkDBConnection(db)):
        logging.info("Daily maintenance started.")
        sendEmailOnMissTapOuts(db)
        logging.info("Daily maintenance ended.")
        
    else:
        logging.error("Database connection failed.")
        logging.critical("Daily maintenance unable to be processed. Please run manually.")
    
    db.close()

#############################################################################
## Biweekly Maintenance main function
#############################################################################
def biweeklyMaintenance():
    # Connect to datacore connection
    db = cfg.connectToDatacore()
    
    # Check database connection integrity
    if (helper.checkDBConnection(db)):
        logging.info("Biweekly maintenance started.")
        fillMissTapOuts(db)

        # Calculate biweekly total hours
        sumBiweeklyWorkHour(db)
    
        logging.info("Biweekly maintenance ended.")
        
    else:
        logging.error("Database connection failed.")
        logging.critical("Bi-weekly maintenance unable to be processed. Please run manually.")
        
    db.close()
    
#############################################################################
## Monthly Maintenance main function
#############################################################################
def monthlyMaintenance():
    # Connect to datacore connection
    db = cfg.connectToDatacore()
    
    # Check database connection integrity
    if (helper.checkDBConnection(db)):
        logging.info("Monthly maintenance started.")
        deleteWorkShift(db)
        insertWorkShift(db)
        logging.info("Monthly maintenance ended.")
        
    else:
        logging.error("Database connection failed.")
        logging.critical("Monthly maintenance unable to be processed. Please run manually.")
    
    db.close()
    
#############################################################################
## Semi-yearly Maintenance main function
#############################################################################
def semiYearlyMaintenance():
    # Connect to datacore connection
    db = cfg.connectToDatacore()
    
    # Check database connection integrity
    if (helper.checkDBConnection(db)):
        logging.info("Semi-yearly maintenance started.")
        helper.insertStatHolidays(db)
        logging.info("Semi-yearly maintenance ended.")
    else:
        logging.error("Database connection failed.")
        logging.critical("Semi-yearly maintenance unable to be processed. Please run manually.")
        db.close()
    
    db.close()
    
#############################################################################
## Function to fill in "NULL" on end date due to miss tap outs.
## @param db -- passing through database connection
#############################################################################
def sendEmailOnMissTapOuts(db):
    # Get yesterday's date in format YYYY-MM-DD
    todayDate = str(helper.get_today_date())
    todayFromRange = todayDate + " 00:00:00"
    todayToRange = todayDate + " 23:59:59"
    
    with db.cursor() as cursor:
        # Scan for events of type RFID with end is NULL on yesterday
        query = "SELECT * FROM `events` WHERE type_id = %d AND (start BETWEEN '%s' AND '%s') AND end is NULL"%(dbEventTypeRFID, todayFromRange, todayToRange)
        cursor.execute(query)
        queryResult = cursor.fetchall()
        
        for row in queryResult:
            queryResultEmployeeID = int(row['employee_id'])
            queryResultEmployeeName = helper.getEmployeeName(db, queryResultEmployeeID)
            queryResultStartDate = helper.convertDatetimeToString(row['start'])
            
            emailing.missedTapOutMailTemplate(queryResultEmployeeID, queryResultEmployeeName, queryResultStartDate)
            logging.info("Missed tap out detected for employee #" +str(queryResultEmployeeID)+": "+ str(queryResultEmployeeName) + " on " + str(queryResultStartDate))
#############################################################################
## Function to fill in "NULL" on end date due to miss tap outs.
## @param db -- passing through database connection
#############################################################################
def fillMissTapOuts(db):
    currentMonth = helper.get_today_date().month
    currentDay = helper.get_today_date().day
    currentYearStr = str(helper.get_today_date().year)
    lastDayOfMonth = helper.getLastDayOfMonth()
    
    # If currentMonth is single digit make it in the form of "0X"
    if (currentMonth >= 1 and currentMonth <= 9):
        currentMonthStr = "0"+str(currentMonth)
        
    # Grab a date string with matching format to the events table in database
    # Check whether we are on the first biweek (currentDay <= 15)
    # Or we are in second biweek (currentDay >= 16)
    if (currentDay <= 15):
        fromDateTimeRange = currentYearStr+"-"+currentMonthStr+"-01"
        toDateTimeRange = currentYearStr+"-"+currentMonthStr+"-15"
        
        fromDateTimeRangeStr = str(fromDateTimeRange) + " 00:00:00"
        toDateTimeRangeStr = str(toDateTimeRange) + " 23:59:59"
        
    elif (currentDay >= 16):
        fromDateTimeRange = currentYearStr+"-"+currentMonthStr+"-16"
        toDateTimeRange = currentYearStr+"-"+currentMonthStr+"-"+str(lastDayOfMonth)
        
        fromDateTimeRangeStr = str(fromDateTimeRange) + " 00:00:00"
        toDateTimeRangeStr = str(toDateTimeRange) + " 23:59:59"
        
    else:
        print("Fatal Error: From/To date range is not valid.")
        logging.critical("Unable to determine date range @ fillMissTapOuts")
        sys.exit(0)
        
    # Check for NULL entry for "end" column in event RFID (event_type = 9)
    with db.cursor() as cursor:
        # Check all RFID entries with start date is within dateRange and end is NULL
        query = "SELECT * FROM `events` WHERE (start BETWEEN '%s' AND '%s') AND (end is NULL) AND (type_id = %d)"%(fromDateTimeRangeStr, toDateTimeRangeStr, dbEventTypeRFID)
        cursor.execute(query)
        queryResult = cursor.fetchall()
        
        # Iterating cursor.fetchall() result
        for row in queryResult:
            # Get employeeID and the day of that row.
            queryResultEmployeeID = int(row['employee_id'])
            dayOfDateStr = datetime.strptime(str(row['start']), '%Y-%m-%d %H:%M:%S').strftime('%A')
            queryResultEmployeeName = helper.getEmployeeName(db, queryResultEmployeeID)
            
            # We will use id_array to store the employee's particular date shift.
            id_array = []
            
            # Get all work shift IDs attached to that specific date.
            # Convert dayOfDateStr to dayOfDateInt to match day_id on work_shifts table.
            if (dayOfDateStr == 'Monday'):
                dayOfDateInt = 1
            elif (dayOfDateStr == 'Tuesday'):
                dayOfDateInt = 2
            elif (dayOfDateStr == 'Wednesday'):
                dayOfDateInt = 3
            elif (dayOfDateStr == 'Thursday'):
                dayOfDateInt = 4
            elif (dayOfDateStr == 'Friday'):
                dayOfDateInt = 5
            elif (dayOfDateStr == 'Saturday'):
                dayOfDateInt = 6
                
            # Check if there is modified shift first, if none use regular work shift end time.
            getDate = datetime.strftime(row['start'], '%Y-%m-%d')
            getDateFromRange = str(getDate)+' 00:00:00'
            getDateToRange = str(getDate)+' 23:59:59'
            
            query5 = "SELECT * FROM `events` WHERE type_id = %d AND employee_id = %d AND (start BETWEEN '%s' AND '%s')"%(dbEventTypeModifiedShift, queryResultEmployeeID, getDateFromRange, getDateToRange)
            modShiftObtained = cursor.execute(query5)
            query5Result = cursor.fetchone()
            
            # If modShiftObtained > 0 then a modified shift exist, we should grab the modified shift end time instead of regular work shift time.
            if (modShiftObtained > 0):
                endDateHypothesis = query5Result['end']
                queryResultID = int(row['id'])
            
                if (row['start'] > endDateHypothesis):
                    print("Start tap time is greater than work shift end time. This is likely possible due to forgot to tap in. Please contact development team.")
                    logging.error('Employee ' + queryResultEmployeeName + ' has a shift start time outside of their regular hour. Please check accordingly.')
                    logging.error('Employee ' + str(queryResultEmployeeName) + ' on events table @ row: ' + str(queryResultID) + ' with start time: ' + str(row['start']) + ' with expected end time: ' + str(endDateHypothesis))
                    emailing.genericEmail('Unable to fill miss tap out for a particular entry', 'Please check logger file for more information. For additional assistance please contact development team.')
                else:
                    sql = "UPDATE `events` SET end = '%s' WHERE id = %d"%(endDateHypothesis, queryResultID)
                    cursor.execute(sql)
                    db.commit()  
            
            else:
                query3 = "SELECT * FROM `work_shifts` WHERE day_id = %d"%(dayOfDateInt)
                cursor.execute(query3)
                query3Result = cursor.fetchall()
                
                # Iterate through query3Result to grab work shift IDs
                for row3 in query3Result:
                    id_array.append(row3['id'])
                
                # Match content of id_array to Employee's work shift id from pivot table.
                query2 = "SELECT * FROM `employee_work_shift` WHERE employee_id = %d"%(queryResultEmployeeID)
                cursor.execute(query2)
                query2Result = cursor.fetchall()
                
                # Iterate and check if query2Result has schedule match.
                for row2 in query2Result:
                    if row2['work_shift_id'] in id_array:
                        # Get the id and get the end time.
                        query4 = "SELECT * FROM `work_shifts` WHERE id = %d"%(row2['work_shift_id'])
                        cursor.execute(query4)
                        query4Result = cursor.fetchone()

                        workShiftEndTime = query4Result['end_time']
                        
                        # End date hypothesis consist of the start time's date and last end date based on work shift.
                        endDateHypothesis = row['start'].strftime("%Y-%m-%d "+workShiftEndTime)
                        queryResultID = int(row['id'])
                    
                        if (row['start'] > datetime.strptime(endDateHypothesis, '%Y-%m-%d %H:%M:%S')):
                            print("Start tap time is greater than work shift end time. This is likely possible due to forgot to tap in. Please contact development team.")
                            logging.error('Employee ' + queryResultEmployeeName + ' has a shift start time outside of their regular hour. Please check accordingly.')
                            logging.error('Employee ' + str(queryResultEmployeeName) + ' on events table @ row: ' + str(queryResultID) + ' with start time: ' + str(row['start']) + ' with expected end time: ' + str(endDateHypothesis))
                            emailing.genericEmail('Unable to fill miss tap out for a particular entry', 'Please check logger file for more information. For additional assistance please contact development team.')
                        else:
                            sql = "UPDATE `events` SET end = '%s' WHERE id = %d"%(endDateHypothesis, queryResultID)
                            cursor.execute(sql)
                            db.commit()
                        
        print("Filling missed tap outs query executed succesfully.")
        logging.info("Missed tap outs succesfully filled.")

#############################################################################
## Function to calculate employee's biweekly work hour.
## @param db -- passing through database connection
#############################################################################
def sumBiweeklyWorkHour(db):
    currentYear = helper.get_today_date().year
    currentYearStr = str(currentYear)
    currentMonth = helper.get_today_date().month
    currentDay = helper.get_today_date().day
    
    lastDayOfMonth = helper.getLastDayOfMonthByInput(currentYear, currentMonth)
    
    # If currentMonth is single digit make it in the form of "0X"
    if (currentMonth >= 1 and currentMonth <= 9):
        currentMonthStr = "0"+str(currentMonth)
    
    # Check whether we are on the first biweek (currentDay <= 15)
    # Or we are in second biweek (currentDay >= 16)
    if (currentDay <= 15):
        fromDateTimeRange = currentYearStr+"-"+currentMonthStr+"-01"
        toDateTimeRange = currentYearStr+"-"+currentMonthStr+"-15"

    elif (currentDay >= 16):
        fromDateTimeRange = currentYearStr+"-"+currentMonthStr+"-16"
        toDateTimeRange = currentYearStr+"-"+currentMonthStr+"-"+str(lastDayOfMonth)
        
    else:
        print("Fatal Error: From/To date range is not valid.")
        logging.critical("Unable to determine date range @ sumBiweeklyWorkHour")
        sys.exit(0)

    try:
        with db.cursor() as cursor:
            # Get all active employees
            activeEmployeesQuery = "SELECT * FROM `employees` WHERE isActive = 1 AND id > 1"
            cursor.execute(activeEmployeesQuery)
            queryResult = cursor.fetchall()
            
            # Iterating cursor.fetchall() result
            for row in queryResult:
                employeeID = int(row['id'])
                employeeName = str(row['name'])
                print(str(employeeID) + " " + employeeName)
                
                # The secondsSum should be reset per user.
                secondsSum = 0
                
                # Iterate through each employee and sum up their total hour within time range
                # Store sum of work hour in a new(?) table
                calculateWorkHourQuery = "SELECT * FROM `events` WHERE (start BETWEEN CAST('%s' AS DATE) AND CAST('%s' AS DATE) AND (type_id = %d) AND (employee_id = %d))"%(fromDateTimeRange, toDateTimeRange, dbEventTypeRFID, employeeID)
                cursor.execute(calculateWorkHourQuery)
                queryResult2 = cursor.fetchall()
                
                for row2 in queryResult2:                    
                    # Begin iterating valid start and end time and convert them to seconds.
                    startTime = helper.convertTimeToSeconds(row2['start'])
                    
                    if (row2['end'] is None):
                        # If the employee does not have a shift scheduled at that time, then send warning to admin and put end time as start time.
                        emailing.scheduleErrorMailTemplate(employeeName, row2['start'].strftime("%Y-%m-%d"))
                        endDateHypothesis = row2['start']
                        sql = "UPDATE `events` SET end = '%s' WHERE id = %d"%(endDateHypothesis, row2['id'])
                        cursor.execute(sql)
                        db.commit()
                        
                    else:
                        endTime = helper.convertTimeToSeconds(row2['end'])
                        userID = int(row2['employee_id'])
                        # Sum up the difference of times.
                        diffTime = endTime - startTime
                        
                        # diffTime will be negative when user taps in out of their
                        # end time on their work shift.
                        # If negative, then ignore and do not add to the sum.
                        if (diffTime > 0 and employeeID == userID):
                            secondsSum += int(diffTime)

                            workTimeStr = helper.convertSecondsToTime(secondsSum)
                            
                            ### create a DB query to input the value.
                            ### Beware of the loop, if we use insert to db function
                            ### it will keep creating multiple entries.
                            ### Best way to do this is if no entry for this specific time range exist:
                            ### then insert. But if it already exist, then update the time instead.

                            # SQL Query to check if the employee already had existing record within the date range
                            checkQuery = "SELECT * FROM `payrolls` WHERE ((employee_id = %d) AND date_from BETWEEN CAST('%s' AS DATE) AND CAST('%s' AS DATE) AND date_to BETWEEN CAST('%s' AS DATE) AND CAST('%s' AS DATE))"%(userID, fromDateTimeRange, toDateTimeRange, fromDateTimeRange, toDateTimeRange)
                            cursor.execute(checkQuery)
                            checkQueryResult = cursor.fetchone()
                            
                            # If no previous record in the date range is found,
                            # then, insert new row into payrolls table.
                            if (checkQueryResult is None):
                                insertPRQuery = "INSERT INTO payrolls (employee_id, date_from, date_to, total_seconds, total_time) VALUES (%d, '%s', '%s', %d, '%s')"%(userID, str(fromDateTimeRange), str(toDateTimeRange), secondsSum, workTimeStr)
                                cursor.execute(insertPRQuery)
                                db.commit()
                                
                                # Get Employee's name from employees table
                                employeeName = helper.getEmployeeName(db, userID)
                                
                                print("Inserting payroll information for " + employeeName + " successful!")
                                
                            # If record exist, then update that entry with the latest work hour sum.
                            else:
                                rowID = checkQueryResult['id']
                                updatePRQuery = "UPDATE payrolls SET total_seconds = %d, total_time = '%s' WHERE employee_id = %d AND id = %d"%(secondsSum, workTimeStr, userID, rowID)
                                cursor.execute(updatePRQuery)
                                db.commit()
                                
                                # Get Employee's name from employees table
                                employeeName = helper.getEmployeeName(db, userID)
                                
                                print("Updating payroll information for " + employeeName + " successful!")

            print("Active Employees: " + str(len(queryResult)))
            emailing.payrollReportMailTemplate(fromDateTimeRange, toDateTimeRange)
    
    except:
        print("Fatal Error: Unable to sum up employee's hour with the date range given.")
        logging.critical("Fatal Error: Unable to sum up employee's hour with the date range given.")
        db.close()
        
#############################################################################
## Delete work shift record that are 3 months old.
## @param db -- passing through database connection
#############################################################################
def deleteWorkShift(db):
    year = helper.get_today_date().year
    currentMonth = helper.get_today_date().month
    
    if (currentMonth >= 4):
        currentMonthMinusThree = currentMonth - 3
    elif (currentMonth == 3):
        currentMonthMinusThree = 12
        year = year - 1
    elif (currentMonth == 2):
        currentMonthMinusThree = 11
        year = year - 1
    elif (currentMonth == 1):
        currentMonthMinusThree = 10
        year = year - 1
        
    date = helper.get_today_date().replace(year = year)
    
    startDate = date.replace(month = currentMonthMinusThree, day = 1)
    endDate = date.replace(month = currentMonthMinusThree, day = helper.getLastDayOfMonthByInput(year, currentMonthMinusThree))

    try:
        with db.cursor() as cursor:
            # Perform SQL Query to delete events of type_id = 1 (WORK SHIFT) between
            # startDate to endDate
            sql = "SELECT * FROM `events` WHERE (start BETWEEN CAST('%s' AS DATE) AND CAST('%s' AS DATE) AND (type_id = %d))"%(startDate, endDate, dbEventTypeWorkShift)
            # Better way to check for number of rows instead of doing COUNT(*)
            rows_count = cursor.execute(sql)
            queryResult = cursor.fetchall()
            
            if (rows_count == 0):
                print("Work shift data from " + str(startDate) + " to " + str(endDate) + " is non existent.")
                print("No deletion is necessary.")
                
            else:
                for row in queryResult:
                    rowID = int(row['id'])
                    
                    deleteSql = "DELETE FROM `events` WHERE id = %d"%(rowID)
                    cursor.execute(deleteSql)
                    db.commit()
                    
                    print("Event ID: " + str(rowID) + " deleted succesfully. (Expired work shift)")

                print("All expired work shifts from " + str(startDate) + " to " + str(endDate) + " deleted succesfully.")
                logging.info("All expired work shifts from " + str(startDate) + " to " + str(endDate) + " deleted succesfully.")
    except:
        print("Error: Unable to delete older work shifts")
        
#############################################################################
## Function to insert work shift based on the last work shift detected
## @param db -- passing through database connection
#############################################################################
def insertWorkShift(db):
    
    currentYear = helper.get_today_date().year
    currentMonth = helper.get_today_date().month
    
    #Initializing Calendar Module
    c = calendar.Calendar(firstweekday=calendar.SUNDAY)
    monthcal = c.monthdatescalendar(currentYear, currentMonth)
    
    getEmployeeWorkAndVacationDates(db, monthcal)

#############################################################################
## Function to insert next month's work shift
## @param db -- passing through database connection
#############################################################################
def insertNextMonthShifts():
    db = cfg.connectToDatacore()
    
    # Check database connection integrity
    if (helper.checkDBConnection(db)):
        currentYear = helper.get_today_date().year
        currentMonth = helper.get_today_date().month
        
        #Initializing Calendar Module
        c = calendar.Calendar(firstweekday=calendar.SUNDAY)
        
        if (currentMonth == 12):
            nextMonth = 1
            nextYear = currentYear + 1
            monthcal = c.monthdatescalendar(nextYear, nextMonth)
        else:
            nextMonth = currentMonth + 1
            monthcal = c.monthdatescalendar(currentYear, nextMonth)
        
        getEmployeeWorkAndVacationDates(db, monthcal)
        
        # Send an e-mail notifier that next month shift is ready.
        emailing.genericEmail('Next Month Shift Ready',
                              'This is a notifier that the next month work shift is ready. Please modify accordingly if necessary.')
        
    else:
        logging.error("Database connection failed.")
        logging.critical("Inserting next month shift maintenance unable to be processed. Please run manually.")
        
    db.close()

#############################################################################
##
#############################################################################
def getEmployeeWorkAndVacationDates(db, monthcal):
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    with db.cursor() as cursor:
        # Get all active employees
        activeEmployeesQuery = "SELECT * FROM `employees` WHERE isActive = 1 AND id > 1"
        cursor.execute(activeEmployeesQuery)
        employeesQueryResult = cursor.fetchall()
        
        # Iterating each employee
        for row in employeesQueryResult:
            employeeID = int(row['id'])
            employeeName = str(row['name'])
            
            # Get the year and month that we want from the calendar.
            # Here we grabbed the 3rd week because sometimes the first week
            # is a data from the previous month.
            fromYear = monthcal[2][1].year
            fromMonth = monthcal[2][1].month
        
            lastDayFromVars = helper.getLastDayOfMonthByInput(fromYear, fromMonth)
            
            if (fromMonth < 10):
                fromRangeStr = str(fromYear)+ "-"+"0"+str(fromMonth)+"-"+"01"
                toRangeStr = str(fromYear)+"-"+"0"+str(fromMonth)+"-"+str(lastDayFromVars)
            else:
                fromRangeStr = str(fromYear)+"-"+str(fromMonth)+"-"+"01"
                toRangeStr = str(fromYear)+"-"+str(fromMonth)+"-"+str(lastDayFromVars)
            
            # Get all vacation dates for the employee based on the passed month.
            vacationDatesQuery = "SELECT * FROM `events` WHERE (type_id = %d AND employee_id = %d) AND (start BETWEEN '%s' AND '%s' OR end BETWEEN '%s' AND '%s')"%(dbEventTypeVacation, employeeID, fromRangeStr, toRangeStr, fromRangeStr, toRangeStr)
            cursor.execute(vacationDatesQuery)
            vacationDatesQueryResult = cursor.fetchall()
            
            vacDaysArray = []
            
            for row in vacationDatesQueryResult:
                sdate = row['start'].date()
                edate = row['end'].date()
                
                delta = edate-sdate

                for i in range(delta.days + 1):
                    day = sdate + timedelta(days=i)
                    vacDaysArray.append(day)
            
            # Sort the array and use it on checkAndCreateWorkShift
            vacDaysArray.sort()
            
            checkEmployeeShiftsQuery = "SELECT * FROM `employee_work_shift` WHERE employee_id = %d"%(employeeID)
            cursor.execute(checkEmployeeShiftsQuery)
            employeeShiftsResult = cursor.fetchall()
            
            # Iterating through employee_work_shift pivot table
            for pivotRow in employeeShiftsResult:
                if (pivotRow['work_shift_id'] is None):
                    print("Work shift is non existent. Please contact Lee's Electronic Development Team.")
                    logging.info("Work shift for employee: " + str(employeeName) + " is non existent.")
                else:
                    print("------------------------------------------------------------------")
                    
                    # Obtain the corresponding shift based on work_shifts table
                    obtainWorkShiftsQuery = "SELECT * FROM `work_shifts` WHERE id = %d"%(pivotRow['work_shift_id'])
                    cursor.execute(obtainWorkShiftsQuery)
                    workShiftsResult = cursor.fetchone()
                    
                    # Grab the day / start_time / end_time of the work shift to be used with Calendar.OBJECT.
                    shiftDay = workShiftsResult['day']
                    shiftStartTime = workShiftsResult['start_time']
                    shiftEndTime = workShiftsResult['end_time']
                    
                    # Grab dates corresponding to the day on workShiftsResult
                    if (shiftDay == "MONDAY"):
                        workDates = [day for week in monthcal for day in week if day.weekday() == calendar.MONDAY]
                    elif (shiftDay == "TUESDAY"):
                        workDates = [day for week in monthcal for day in week if day.weekday() == calendar.TUESDAY]
                    elif (shiftDay == "WEDNESDAY"):
                        workDates = [day for week in monthcal for day in week if day.weekday() == calendar.WEDNESDAY]
                    elif (shiftDay == "THURSDAY"):
                        workDates = [day for week in monthcal for day in week if day.weekday() == calendar.THURSDAY]
                    elif (shiftDay == "FRIDAY"):
                        workDates = [day for week in monthcal for day in week if day.weekday() == calendar.FRIDAY]
                    elif (shiftDay == "SATURDAY"):
                        workDates = [day for week in monthcal for day in week if day.weekday() == calendar.SATURDAY]
                    else:
                        print("FATAL ERROR: Day associated to a work shift is invalid.")
                        logging.critical("FATAL ERROR: Day associated to a work shift is invalid.")
                        emailing.workShiftErrorMailTemplate()
                        emailing.fatalErrorMailTemplate()
                        sys.exit(0)
                    
                    # Now that the dates are obtained, input the date and the work shift time into events table
                    checkAndCreateWorkShift(db, workDates, vacDaysArray, dbEventTypeWorkShift, shiftStartTime, shiftEndTime, pivotRow['employee_id'], employeeName, cursor)
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

#############################################################################
## Helper/Refactoring function to perform data check and insert when inserting
## work shift.
## @param db -- passing through database connection
## @param workDates -- the list of dates corresponding to the employee's work day.
## @param vacDaysArray -- the list of vacation dates corresponding to the specified employee.
## @param dbEventTypeWorkShift -- static variable obtained to match with the database
##     database "event_types"
## @param shiftStartTime -- employee's assigned shift start time.
## @param shiftEndTime -- employee's assigned shift end time.
## @param employeeID -- employee's ID.
## @param employeeName -- employee's name associated with the ID.
## @param cursor -- passing in database cursor for less declaration
#############################################################################
def checkAndCreateWorkShift(db, workDates, vacDaysArray, dbEventTypeWorkShift, shiftStartTime, shiftEndTime, employeeID, employeeName, cursor):
    try:
        beginningTime = "00:00:00"
        endTime = "23:59:59"
      
        for date in workDates:
            
            startShiftFormat = str(date) + " " + str(shiftStartTime)
            endShiftFormat = str(date) + " " + str(shiftEndTime)
            
            startTimeFormat = str(date) + " " + str(beginningTime)
            endTimeFormat = str(date) + " " + str(endTime)
            
            # Check if there is already work shift in each of this date.
            # If there is, then do not insert, else insert work shift.
            sql3 = "SELECT * FROM `events` WHERE ((employee_id = %d AND (type_id = %d OR type_id BETWEEN %d AND %d)) OR (type_id = %d)) AND start BETWEEN '%s' AND '%s'"%(employeeID, dbEventTypeWorkShift, dbEventTypeVacation, dbEventTypeDayOff, dbEventTypeHoliday, startTimeFormat, endTimeFormat)
            sql3RowCount = cursor.execute(sql3)
            
            if(sql3RowCount == 0 and (date not in vacDaysArray)):
                insertPRQuery = "INSERT INTO events (type_id, employee_id, start, end) VALUES (%d, %d, '%s','%s')"%(dbEventTypeWorkShift, employeeID, startShiftFormat, endShiftFormat)
                cursor.execute(insertPRQuery)
                db.commit()
            else:
                print("Work shift already exist for: " + str(employeeName) + " on the date: " + str(date))
                
        print("Work shift for " + employeeName + " for the month created succesfully.")
    
    except:
        print("Fatal Error: Unable to create employee work shift.")
        logging.critical("Fatal Error: Unable to create employee work shift.")
        emailing.fatalErrorMailTemplate()
        sys.exit(0)        