#!/usr/bin/python
#########################################################################################
## Hour Logging System force monthly maintenance file.
## This file forces monthly maintenance to run upon file calling (execution)
##
## Monthly maintenance will delete work shifts that are 3 months old and inserting
## employee's work shift.
##
## Created by: Tony Lim
#########################################################################################

import maintenance

maintenance.monthlyMaintenance()