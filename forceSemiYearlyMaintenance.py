#!/usr/bin/python
#########################################################################################
## Hour Logging System force semi-yearly maintenance file.
## This file forces semi-yearly maintenance to run upon file calling (execution)
##
## Semi-yearly maintenance is responsible for inserting statutory holidays,
## and updating it accordingly.
##
## Created by: Tony Lim
#########################################################################################

import maintenance

maintenance.semiYearlyMaintenance()