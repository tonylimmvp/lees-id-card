#!/usr/bin/python
#########################################################################################
## Hour Logging System main running file.
## This file is the production version, emulating DataCore connection.
## Note: This software is designed to run on Python 3. As such, MySQLdb and
##       SimpleMFRC522 library cannot be used as they are not compatible
##       with Python 3.
##
## This RFID Reader only takes 13.56MHz frequency cards/tags. (#15365 / 15050)
## A 125 KHz tag (#150321) is also working with the reader.
## However 125 KHz button tag (#15020) does not work.
##
## Possible replacement for MySQLdb is PyMySQL please see documentation below:
## https://pypi.org/project/PyMySQL/#installation 
##
## Created by: Tony Lim
## Credits: Richard Sun, Hazel Yang, Gabriel Chiu, Haotian Ye, Bhavik Maisuria,
##          Duncan Yee, Leon Cao, and Lee's Electronic Components (2005) Ltd.
##          For initially starting the project.
#########################################################################################
import time
from time import sleep
from datetime import datetime, timedelta, date
import RPi.GPIO as GPIO
import mfrc522
import I2C_LCD_driver
import holidays
import pymysql.cursors
import calendar
import cfg
import maintenance
import helperfunctions as helper
import sys
import emailing
import logging

# Create an object of the class MFRC522
MIFAREReader = mfrc522.MFRC522()

# Create an object for the I2C 2x16 LCD.
mylcd = I2C_LCD_driver.lcd()

# flags for backlight control (used as state)
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

# Set original state of backlight to be on
state = LCD_BACKLIGHT

# Setup the GPIO pin 8 for the button interface
GPIO.setwarnings(False) #Ignore GPIO warnings
GPIO.setmode(GPIO.BOARD) #Use physical pin numbering

GPIO.setup(36,GPIO.OUT)

# Set pin 8 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(8, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# Initialize timeout default time in seconds:
timeoutTime = 10

# Enable logging and determine file.
logging.basicConfig(filename='logger.log',level=logging.DEBUG, format='%(asctime)s %(filename)s - %(levelname)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S')

# Set the time to perform biweekly maintenance schedule
# Biweek Maintenance default trigger time:
# 15th / last day of month @ 9:00 PM
biweekMaintenanceDayTrigger = "15"
biweekMaintenanceHourTrigger = "21"
biweekMaintenanceMinuteTrigger = "00"

# Set the time to perform monthly maintenance schedule
# Monthly Maintenance default trigger time:
# First day of the month @ 7:00 AM
monthlyMaintenanceDayTrigger = "01"
monthlyMaintenanceHourTrigger = "07"
monthlyMaintenanceMinuteTrigger = "00"

# Set the time to perform semi yearly maintenance schedule
# Biweek Maintenance default trigger time:
# June 30 / Dec 30 @ 11:00 PM
semiYearlyMaintenanceMonth1Trigger = "06"
semiYearlyMaintenanceMonth2Trigger = "12"
semiYearlyMaintenanceDayTrigger = "30"
semiYearlyMaintenanceHourTrigger = "23"
semiYearlyMaintenanceMinuteTrigger = "00"

# Set the time to input next month work shift
# Default trigger time:
# 4th monday of the monthly calendar @ 8:00 AM
currentYear = helper.get_today_date().year
currentMonth = helper.get_today_date().month

c = calendar.Calendar(firstweekday=calendar.SUNDAY)
monthcal = c.monthdatescalendar(currentYear, currentMonth)
mondayDates = [day for week in monthcal for day in week if day.weekday() == calendar.MONDAY]
fourthMonday = str(mondayDates[3].day)

nextMonthShiftDayTrigger = fourthMonday
nextMonthShiftHourTrigger = "08"
nextMonthShiftMinuteTrigger = "00"

# Set the time to trigger daily maintenance
# Default trigger time:
# Everyday at 8:00 PM
dailyMaintenanceHourTrigger = "20"
dailyMaintenanceMinuteTrigger = "00"

#############################################################################
## Function to make the buzzer make sound
## @param Time  Time in second(s) for how long the buzzer will buzz
#############################################################################
def buzzer(Time):
    GPIO.output(36, GPIO.HIGH)
    time.sleep(Time)
    GPIO.output(36, GPIO.LOW)

#############################################################################
## Function to reset the LCD Screen to it's default state.
## @param second_line   Text to output into the second line.
## @param state         state of backlight. Default is LCD_BACKLIGHT
## First line will show date and time ex: 2020-01-03 16:15 (16 characters)
## Second line will show text inputted into @param second_line
#############################################################################
def lcd_time_reset(second_line):
    currentTime = datetime.now()
    date = currentTime.strftime("%Y-%m-%d")  
    clock_time = currentTime.strftime("%H:%M")
    lcdDisp(date + " " + clock_time + second_line)

#############################################################################
## This Function will take a string input and display it on the LCD screen.
## 0-15 characters will be shown on first line.
## 16(15+1)-31 characters will be shown on the second line.
## @param text -- Displays the text to be outputted into the LCD screen.
#############################################################################
def lcdDisp(text):
    state = LCD_BACKLIGHT
    is_cleared = False
    mylcd.lcd_clear(state)
    if len(text) > 32:
        count = len(text)/32    ##Determines the number of sections for the display
        for n in range(0,count+4):  ##Error will show if overflow on characters.
            mylcd.lcd_display_string(state, text[15*n:15*(n+1)], 1)
            mylcd.lcd_display_string(state, text[15*(n+1):15*(n+2)],2)
            if is_cleared == True:
                lcd.lcd_clear(state)
                
    elif len(text) <= 32 and len(text) > 16:
        mylcd.lcd_display_string(state,text[:16], 1)
        mylcd.lcd_display_string(state,text[16:], 2)
        if is_cleared == True:
            mylcd.lcd_clear(state)
                
    elif len(text) <= 16:
        mylcd.lcd_display_string(state,text, 1)
        sleep(0.001)
        if is_cleared == True:
            mylcd.lcd_clear(state)

#############################################################################
## Function to be called when the button is pushed and is in push to scan state
#############################################################################
def button_callback_pushtoscan():
    try:
        #lcdDisp("Button pushed.")
        recordTapInAndOut()
    except:
        lcdDisp("Error on button push. Please try again.")
        logging.error("Error on button push.")
        time.sleep(3)
        pushToScanState()
    
#############################################################################
## Push to Scan state declaration
## Initial Default State
#############################################################################
def pushToScanState():
    GPIO.cleanup()
    runFlag = True
    maintenanceFlag = False
    
    lastMin = helper.getCurrentMinute()
    currentMonth = helper.get_today_datetime().strftime("%m")
    currentDay = helper.get_today_datetime().strftime("%d")
    currentHour = helper.getCurrentHour()
    currentSecond = helper.getCurrentSecond()

    lastDayOfMonth = helper.getLastDayOfMonth()

    ## Setup the GPIO pin 8 for the button interface
    GPIO.setwarnings(False) #Ignore GPIO warnings
    GPIO.setmode(GPIO.BOARD) #Use physical pin numbering

    GPIO.setup(36,GPIO.OUT)

    ## Set pin 8 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(8, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    
    lcd_time_reset("Push to Scan")
    
    # If button is pushed; GPIO.input(8) = 1
    # Main infinite loop of the program.
    while (runFlag):
        if (GPIO.input(8) == 0):
            currentMin = helper.getCurrentMinute()
            
            # Biweekly Maintenance Trigger
            if((currentDay == biweekMaintenanceDayTrigger or currentDay == lastDayOfMonth) and (currentHour == biweekMaintenanceHourTrigger and currentMin == biweekMaintenanceMinuteTrigger and currentSecond == "00")):
                print("Bi-Weekly maintenance start now " + str(helper.get_today_datetime()))
                maintenance.biweeklyMaintenance()
                print("Bi-Weekly maintenance end now " + str(helper.get_today_datetime()))

                # Create 2 second delay to prevent double trigger
                time.sleep(2)
                break
        
            # Monthly Maintenance Trigger
            if(currentDay == monthlyMaintenanceDayTrigger and currentHour == monthlyMaintenanceHourTrigger and currentMin == monthlyMaintenanceMinuteTrigger and currentSecond == "00"):
                print("Monthly maintenance start now " + str(helper.get_today_datetime()))
                maintenance.monthlyMaintenance()
                print("Monthly maintenance end now " + str(helper.get_today_datetime()))

                # Create 2 second delay to prevent double trigger
                time.sleep(2)
                break
            
            # Semi-yearly Maintenance Trigger
            if((currentMonth == semiYearlyMaintenanceMonth1Trigger or currentMonth == semiYearlyMaintenanceMonth2Trigger) and (currentDay == semiYearlyMaintenanceDayTrigger) and (currentHour == semiYearlyMaintenanceHourTrigger and currentMin == semiYearlyMaintenanceMinuteTrigger and currentSecond == "00")):
                print("Semi-yearly maintenance start now " + str(helper.get_today_datetime()))
                maintenance.semiYearlyMaintenance()
                print("Semi-yearly maintenance completed at: " + str(helper.get_today_datetime()))

                # Create 2 second delay to prevent double trigger
                time.sleep(2)
                break
            
            # Fill in next month's shift trigger
            if((currentDay == nextMonthShiftDayTrigger) and (currentHour == nextMonthShiftHourTrigger and currentMin == nextMonthShiftMinuteTrigger and currentSecond == "00")):
                print("Next month shift maintenance start now " + str(helper.get_today_datetime()))
                logging.info('Next month shift maintenance started.')
                maintenance.insertNextMonthShifts()
                print("Next month shift maintenance completed at: " + str(helper.get_today_datetime()))
                logging.info('Next month shift maintenance ended.')

                # Create 2 second delay to prevent double trigger
                time.sleep(2)
                break
            
            # Daily Maintenance Trigger:
            if(currentHour == dailyMaintenanceHourTrigger and currentMin == dailyMaintenanceMinuteTrigger and currentSecond == "00"):
                print("Daily maintenance start now " + str(helper.get_today_datetime()))
                maintenance.dailyMaintenance()
                print("Daily maintenance completed at: " + str(helper.get_today_datetime()))

                # Create 2 second delay to prevent double trigger
                time.sleep(2)
                break
            
            if (currentMin != lastMin):
                runFlag = False
                break
            else:
                #Infinite loop continue
                continue
        
        elif (GPIO.input(8) == 1):
            runFlag = False
            button_callback_pushtoscan()
            break
        
        else:
            runFlag = False
            break
        
    # Create infinite loop.
    pushToScanState()
        
#############################################################################
## Timeout state declaration
## Activated after X seconds of inactivity
## Reset to default state (Push to Scan) when triggered
#############################################################################    
def timeoutState():
    buzzer(0.5)
    
    #Fix to stop readCard activating twice on button push.
    GPIO.setup(8,GPIO.IN,GPIO.PUD_DOWN)
    GPIO.remove_event_detect(8)
    
    # Perform Timeout Noticeswhen is lee's electronic founded
    print("Timeout triggered!")
    lcdDisp("Timeout!")
    logging.error('Timeout state triggered.')
    time.sleep(3)
    
    # Return to default state
    pushToScanState()
    
#############################################################################
## Error timeout state declaration
## Reset to default state (Push to Scan) when triggered
## @param errMsg -- Error message to be displayed. (Max 32 chars)
#############################################################################    
def errorTimeoutState(errMsg):
    buzzer(0.5)
    
    # Check to prevent overflow:
    if (len(errMsg) > 32):
        lcdDisp("Error: Character Overflow!")
        logging.error('Error Timeout state character overflow.')
    else:        
        # Perform Timeout Notices
        print("Error timeout: " + errMsg)
        lcdDisp(errMsg)
        logging.error('Error Timeout state triggered.')
        time.sleep(5)
        
        # Return to default state
        time.sleep(0.5)
        pushToScanState()

#############################################################################
## Checking and obtaining Employee ID based on their name
## @param full_name -- Employee's full name.
## @param cursor -- Passing in through db connection. (To bypass redeclaring
##     db connection)
#############################################################################
def getEmployeeID(full_name, cursor):
    try:
        cursor.execute("SELECT id FROM `employees` WHERE name = '%s'"%(full_name))
        queryResult=cursor.fetchall()
        employee_id = (queryResult[0]['id'])
        return employee_id
    
    except:
        print("Unable to obtain employee ID by given parameter.")
        logging.error('Unable to obtain employee name by given parameter.')
        
#############################################################################
## Checking and obtaining Employee ID based on their name
## @param cursor -- Passing in through db connection. (To bypass redeclaring
##     db connection)
#############################################################################
def getEmployeeIDByRFID(cursor):
    try:
        #Get card ID.
        cardID = readCard()
        
        cursor.execute("SELECT id, name, card_id FROM `employees` WHERE card_id = '%s'"%(cardID))
        queryResult=cursor.fetchall()
        employee_id = (queryResult[0]['id'])
        print(employee_id)
        return employee_id
    
    except:
        lcdDisp("No employee found")
        print("Unable to obtain employee ID by given parameter.")
        logging.error('Unable to obtain employee name by given parameter.')

#############################################################################
## Record Tap In And Out
## @param full_name -- Employee's full name.
## @param cursor -- Passing in through db connection. (To bypass redeclaring
##     db connection)
#############################################################################
def recordTapInAndOut():
    try:
        db = cfg.connectToDatacore()
    except:
        print("Fatal Error: Database connection failed.")
        print("Please check the connection information supplied.")
        print("Exiting program.")
        lcdDisp("Fatal Error: DB CON")
        logging.critical('Unable to access database connection.')
        
        # Hotfix to fix freezing bug.
        logging.info("Reverting state back to default (push to scan)")
        pushToScanState()
        
    # Set RFID Event type to match the database.
    dbEventTypeRFID = 9    
    
    # Get Today's Date in string format, e.g.: 2020-01-29 14:23:06
    # This is used to match the database format
    todayDateTime = helper.get_today_datetime().strftime("%Y-%m-%d %H:%M:%S")
    todayDate = helper.get_today_datetime().strftime("%Y-%m-%d")
        
    try:
        with db.cursor() as cursor:
            # Get employee ID via RFID.
            employeeID = getEmployeeIDByRFID(cursor)
            employeeName = helper.getEmployeeName(db, employeeID)
            
            # Get Current Date Time
            currentDateTime = helper.get_today_datetime().strftime("%Y-%m-%d %H:%M:%S")

            # Tapping in on a new day.
            query = """(SELECT * FROM `events` WHERE ((DATE(start) = '%s') and (employee_id = %d) and (type_id = %d)) ORDER BY id DESC LIMIT 1)"""%(todayDate, employeeID, dbEventTypeRFID)
            cursor.execute(query)
            queryResult = cursor.fetchone()
            if queryResult is None:
                sql = "INSERT INTO events (type_id, employee_id, start) VALUES (%d, %d, '%s')"%(dbEventTypeRFID, employeeID, currentDateTime)
                cursor.execute(sql)
                db.commit()
                
                # Display message of tap in/out and revert state back to default
                lcdDisp("Welcome " + employeeName)
                print("Welcome " + employeeName + " on " + currentDateTime)
                logging.debug("Welcome " + employeeName)
                time.sleep(3)
                pushToScanState()
                
            # An entry already exist, but never tapped out.
            # If the day is the same, then it must be a tap out.
            elif ((queryResult['start'] is not None) and (queryResult['end'] is None)):
                queryResultID = queryResult['id']
                sql2 = "UPDATE events SET end = '%s' WHERE id = %d"%(currentDateTime, queryResultID)
                cursor.execute(sql2)
                db.commit()
                
                # Display message of tap in/out and revert state back to default
                lcdDisp("Goodbye " + employeeName)
                print("Goodbye " + employeeName + " on " + currentDateTime)
                logging.debug("Goodbye " + employeeName)
                time.sleep(3)
                pushToScanState()
                
            # If queryResult returns a data with both start and end is not NULL
            # then multiple tap detected for the same day.
            # The next tap should be a tap in again (create a new row)
            elif ((queryResult['start'] is not None) and (queryResult['end'] is not None)):
                sql = "INSERT INTO events (type_id, employee_id, start) VALUES (%d, %d, '%s')"%(dbEventTypeRFID, employeeID, currentDateTime)
                cursor.execute(sql)
                db.commit()
                
                # Display message of tap in/out and revert state back to default
                lcdDisp("Welcome " + employeeName)
                print("Welcome " + employeeName + " on " + currentDateTime)
                logging.debug("Welcome " + employeeName)
                time.sleep(3)
                pushToScanState()
                
            # Error: This else statement should never called unless there is a fatal error
            else:
                lcdDisp("Error: Tapping in/out")
                print("Fatal Error: Case in tap in and out condition is breached.")
                logging.warning("Error: Tapping in/out")
                time.sleep(3)
                errorTimeoutState("Fatal Error: recordTapInAndOut")

    except:
        lcdDisp("Error: Tapping in/out")
        print("Error: Tapping in/out; Possibly logic/syntax error or user not found")
        logging.warning("Error: Tapping in/out; Possibly logic/syntax error or user not found")
        time.sleep(2)
        emailing.fatalErrorMailTemplate()
        errorTimeoutState("Error: recordTapInAndOut")
        
#############################################################################
## Card Reading State
#############################################################################
def readCard():
    # Welcome message
    print("Tap card to the reader.")
    
    # Bug fix when calling this function manually.
    lcdDisp("Tap Card!")
    logging.info("Tapping Card Activated.")
    
    initTime = int(time.time())
    nowTime = int(time.time())
    
    try:
        # Loop until X (check variable: timeoutTime) seconds expire
        while (initTime + timeoutTime != nowTime):
            
            # Used to keep incrementing nowTime to calculate elapsed time.
            nowTime = int(time.time())

            print("initTime: " + str(initTime))
            print("nowTime: " + str(nowTime))

            # Scan for cards
            (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

            # Get the UID of the card
            (status,uid) = MIFAREReader.MFRC522_Anticoll()

            # If we have the UID, continue
            if status == MIFAREReader.MI_OK:

                # Trigger Buzzer to Acknowledge Reading
                buzzer(0.1)
                
                # Print UID into LCD display and program.
                cardID = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
                print("UID: "+cardID)
                logging.info("UID: "+cardID)
                
                lcdDisp(cardID)
                
                # Debounce fix
                GPIO.setup(8,GPIO.IN,GPIO.PUD_DOWN)## software debounce, set gpio not to be floating
                GPIO.remove_event_detect(8)
          
                # Allow 3 seconds to show read data, proceed to default state.
                time.sleep(3)
                
                return cardID

        # Call timeout state
        if (initTime + timeoutTime == nowTime):
            timeoutState()
        
    except KeyboardInterrupt:
        GPIO.cleanup()

############################################################################
## MAIN LOOP FUNCTION
############################################################################
# Let buzzer beeps for 0.2 second on startup
buzzer(0.3)

try:
    # Perform maintenance upon startup
    lcdDisp("Initializing...")
    logging.debug('System initializing...')
    time.sleep(2)

    # Sound Buzzer to denote that system is ready for use.
    buzzer(0.1)
    time.sleep(0.1)
    buzzer(0.1)
    time.sleep(0.1)
    buzzer(0.1)
    time.sleep(0.1)

    print("--------------------------------------------------------")
    print("System initialized succesfully.")
    print("--------------------------------------------------------")
    logging.debug('System initialized succesfully.')

    # Initialize default state
    pushToScanState()
except:
    lcdDisp("Initialization Error")
    emailing.genericEmail("Unsuccesful Initialization.", "The system is unable to initialize properly. Please contact development team.")
    logging.critical('Initialization failed. Please ensure the server is running.')
    sys.exit(0)
