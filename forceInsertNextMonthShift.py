#!/usr/bin/python
#########################################################################################
## Hour Logging System to force inserting next month shift.
## This file forces inserting next month shift to run upon file calling (execution)
##
## Created by: Tony Lim
#########################################################################################

import maintenance

maintenance.insertNextMonthShifts()